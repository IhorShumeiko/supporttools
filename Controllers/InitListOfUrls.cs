﻿using Newtonsoft.Json;
using SupportTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SupportTools.Controllers
{
    public class InitListOfUrls
    {
        public List<UrlServiceOrSite> UrlsOfServices = new List<UrlServiceOrSite>();
        public List<UrlServiceOrSite> UrlsOfSites = new List<UrlServiceOrSite>();

        public List<UrlServiceOrSite> InitList()
        {
            var path = @"..\wwwroot\Files\UrlsOfServices.json";
            List<UrlServiceOrSite> urls = null;
            using (StreamReader file = File.OpenText(path))
            {
                JsonSerializer serializer = new JsonSerializer();
                urls = (List<UrlServiceOrSite>)serializer.Deserialize(file, typeof(List<UrlServiceOrSite>));
            }

            foreach (UrlServiceOrSite u in urls)
            {
                Console.WriteLine(u.Name + " : " + u.Url);
            }
            return urls;
        }


    }
}
