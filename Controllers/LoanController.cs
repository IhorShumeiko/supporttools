﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupportTools.Data;
using SupportTools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportTools.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoanController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public LoanController(ApplicationDbContext context)
        {
            _context = context;
        }



        [HttpGet]
        public async Task<IEnumerable<Loan>> Get()
        {
            var loans = await _context.Loans.ToListAsync();
            return loans;
        }

        // GET api/<LoanController>/5
        [HttpGet("{Status}")]
        public async Task<IEnumerable<Loan>> Get(int status)
        {
            var loans = from loan in await _context.Loans.ToListAsync()
                        where loan.Status == status
                        select loan;

            return loans;
        }

    }
}
