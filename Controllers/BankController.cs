﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SupportTools.Data;
using SupportTools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SupportTools.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BankController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public BankController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: api/<BankController>
        [HttpGet]
        public async Task<IEnumerable<Bank>> Get()
        {
            var banks = await _context.Banks.ToListAsync();
            return banks;
        }

        // GET api/<BankController>/5
        [HttpGet("{id}")]
        public async Task<Bank> Get(string id)
        {
            var banks = await _context.Banks.ToListAsync();
            Bank bank = null;
            foreach (Bank b in banks)
            {
                if (id == b.Id)
                {
                    bank = b;
                }
            }
            return bank;
        }

        // POST api/<BankController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<BankController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<BankController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
