﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SupportTools.Data;
using SupportTools.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;


namespace SupportTools.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DeserializeMyUrlsController 
    {
        const int MINUTE_OF_REQUEST = 1;
        const string botChatId = "-1001474700467";
        List<Site> services= new List<Site>();
        List<Site> sites = new List<Site>();
        private int countOntime = 0;

        public List<UrlServiceOrSite> DesFromFile(string fileName)
        {
            Console.WriteLine("Hello! from  DesFromFile(string fileName)!!!");
            string filePath = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, @"wwwroot\Files\"))  + fileName;
            string _dataJson = File.ReadAllText(filePath);
            var _data = JsonConvert.DeserializeObject<List<UrlServiceOrSite>>(_dataJson);
            return _data;
        }

        [HttpGet]
        public IEnumerable<Site> Get()
        {
            List<Site> sites = new List<Site>();
            var urls = DesFromFile("UrlsOfServices.json");
            foreach (UrlServiceOrSite u in urls)
            {
                Console.WriteLine("Hello! from IEnumerable<Site> Get()");
                MyHttpRequestResponse rr = new MyHttpRequestResponse(u.Url);
                sites.Add(new Site(u.Name, u.Url, rr.StatusCode, DateTime.Now, DateTime.Now));

            }
            return sites;
        }

        [HttpGet("{fileName}")]
        public IEnumerable<Site> Get(string fileName)
        {
            Console.WriteLine("Hello! from PostRadio");
            List<Site> sites = new List<Site>();
            var urls = DesFromFile(fileName + ".json");
            foreach (UrlServiceOrSite u in urls)
            {
                Console.WriteLine("Hello! from PostRadio");
                MyHttpRequestResponse rr = new MyHttpRequestResponse(u.Url);
                sites.Add(new Site(u.Name, u.Url, rr.StatusCode, DateTime.Now, DateTime.Now));
            }
            return sites;
        }

        [HttpPost("{valueOnOff}")]
        public IEnumerable<Site> PostRadio(string valueOnOff)
        {
            Console.WriteLine("Hello!!!!! from PostRadio");
            RunCheck(valueOnOff);
            return this.sites;
        }

        //[HttpGet("{valueOnOff}")]
        private void RunCheck(string valueOnOff)
        {

            Console.WriteLine("Hello! from GetOnOff");
            const int interval60Minutes = MINUTE_OF_REQUEST * 60000; // milliseconds
            System.Timers.Timer checkForTime = new System.Timers.Timer();
            checkForTime.Interval = interval60Minutes;
            checkForTime.Elapsed += OnTimedEventA;
            //checkForTime.Enabled = true;

            if (valueOnOff.Equals("on"))
            {
                checkForTime.Enabled = true;
            }

            if (valueOnOff.Equals("off"))
            {
                checkForTime.Enabled = false;
                countOntime = 0;
            }

            Console.WriteLine("Hello!!!!!! from GetOnOff");
            //return this.services;
        }

        private async void OnTimedEventA(Object source, System.Timers.ElapsedEventArgs e)
        {
            var urlsites = DesFromFile("UrlsOfSites.json");
            foreach (UrlServiceOrSite u in urlsites)
            {
                MyHttpRequestResponse rr = new MyHttpRequestResponse(u.Url);
                this.sites.Add(new Site(u.Name, u.Url, rr.StatusCode, DateTime.Now, DateTime.Now));
                if (rr.StatusCode != 200)
                {
                    await Task.Run(() => sendAlarmToBotMyCredit(sites.Last()));
                    
                }
            }

            var urlservises = DesFromFile("UrlsOfServices.json");
            foreach (UrlServiceOrSite u in urlservises)
            {
                MyHttpRequestResponse rr = new MyHttpRequestResponse(u.Url);
                this.services.Add(new Site(u.Name, u.Url, rr.StatusCode, DateTime.Now, DateTime.Now));
                if (rr.StatusCode != 200)
                {
                    Console.WriteLine("Hello! from OnTimedEventA  " + countOntime);
                    await Task.Run(() => sendAlarmToBotMyCredit(services.Last()));
                }
            }
        
            ++countOntime;
            Console.WriteLine("Hello!!!!!! from OnTimedEventA  " + countOntime);
        }

        private void sendAlarmToBotMyCredit(Site s)
        {
            Console.WriteLine("Hello! from sendAlarmToBotMyCredit!");
            JsonObjForSender jObj = new JsonObjForSender(s, botChatId);
            var jsonContent = JsonConvert.SerializeObject(jObj);
            var baseUrlBot = botChatId + "/json_value";
            MyGetPostRequestResponse rr = new MyGetPostRequestResponse(baseUrlBot, "POST", jsonContent);
            rr.MyGetResponse();
            Console.WriteLine("Hello!!!!! from sendAlarmToBotMyCredit!");
        }

    }
}
