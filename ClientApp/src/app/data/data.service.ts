import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';



@Injectable()
export class DataService {

  private url = "api/bank";

  constructor(private http: HttpClient) {
  }

  getBanks() {
    return this.http.get(this.url);
  }

  getBank(id: string) {
    return this.http.get(this.url + '/' + id);
  }

  
  deleteBank(id: string) {
    return this.http.delete(this.url + '/' + id);
  }
}
