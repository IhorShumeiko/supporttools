import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-counter-component',
  templateUrl: './counter.component.html'
})
export class CounterComponent {
  public currentCount: string = "sssss";
  public counter: Counter;
  public paramText: string;
  public loan: any;

  constructor(private _activatedRoute: ActivatedRoute, private _router: Router) { }

  public incrementCounter(c: string) {
    this.currentCount = c;
  }

  onBackButtonClick() {
    this._router.navigate(['/loan']);
  }

 
}

interface Counter { }
