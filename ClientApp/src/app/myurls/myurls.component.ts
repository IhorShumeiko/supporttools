import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NgForm } from '@angular/forms';


@Component({
  selector: 'app-myurls',
  templateUrl: './myurls.component.html',
  styleUrls: ['./myurls.component.scss'],
})
export class MyurlsComponent {
  public urls: Url[];
  public radio: string = 'on';

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http.get<Url[]>(baseUrl + 'api/deserializemyurls').subscribe(result => {
      this.urls = result;
    }, error => console.error(error));
  }

  public getUrlsFromFile(fileName: string) {
    this.http.get<Url[]>('api/deserializemyurls/' + fileName).subscribe(result => {
      this.urls = result;
      console.info('Console result:'+result);
    }, error => console.error(error));
  }

  public postUrlsOnOff(valueOnOff: string) {
    return this.http.post('api/deserializemyurls/PostRadio/', valueOnOff);
  }

  public startAutoCheck(valueOnOff: string) {
    console.log('hello from start : ' + valueOnOff);
    return this.http.post('api/deserializemyurls/', valueOnOff);
  }

}

interface Url { }
