import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
})

export class BankComponent  {
  
  banks: Bank[];

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    http.get<Bank[]>(baseUrl + 'api/bank').subscribe(result => {
      this.banks = result;
    }, error => console.error(error));
  }
 

}

interface Bank { }
