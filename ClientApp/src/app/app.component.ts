import { Component } from '@angular/core';
import { BankComponent } from './bank/bank.component';
import { DataService } from './data/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
 
})
export class AppComponent  {
  title = 'app';
  
}
