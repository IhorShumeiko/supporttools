import { Component, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-loan',
  templateUrl: './loan.component.html'
})

export class LoanComponent {
  public loans: Loan[];
  public paramText: string;
  

  constructor(private http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    this.http.get<Loan[]>(baseUrl + 'api/loan').subscribe(result => {
      this.loans = result;
      console.info(result);
    }, error => console.error(error));
  }

  public getLoansOfStatus(status: string) {
    this.http.get<Loan[]>('api/loan/' + status).subscribe(result => {
      this.loans = result;
      console.info(result);
    }, error => console.error(error));

  }

}

interface Loan {}

