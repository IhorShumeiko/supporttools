﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Config;

namespace SupportTools
{
    public static class Logger
    {
        private static ILog log = LogManager.GetLogger("LOGGER");

        //Возвращает объект, через который будет осуществляться логирование
        public static ILog Log
        {
            get { return log; }
        }

        //Инициализация логера
        public static void InitLogger()
        {
            //Считывание конфигурации логера из файла
            XmlConfigurator.Configure(new System.IO.FileInfo("C:\\Logs\\TestConf.xml"));
        }
        //Вспомогательный метод записи в лог массива данных (в шестнадцатеричном виде) в текстовым пояснением
        public static void WriteToLog(string aText, byte[] aDatas)
        {
            if (aDatas == null)
            {
                Logger.Log.Debug(aText);
                return;
            }

            StringBuilder builder = new StringBuilder();

            foreach (byte oneByte in aDatas)
            {
                builder.Append(string.Format("{0:X2} ", oneByte));
            }

            Logger.Log.Debug(string.Format("\t{0}:\t{1}", aText, builder.ToString()));
        }
    }
}
    
