﻿using System;
using System.Net;

namespace SupportTools.Data
{
    public class MyHttpRequestResponse
    {
        public int StatusCode { get; private set; }
        private string Url { get; set; }

        public MyHttpRequestResponse(string url)
        {
            this.Url = url;
            RequestResponseSite(url);
        }

        public override string ToString()
        {
            return $"Code = {StatusCode} from {Url} ";
        }

        private void RequestResponseSite(String url)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                this.StatusCode = (int)response.StatusCode;
                response.Close();
            }
            catch (WebException ex)
            {
                if (ex.Response != null) { this.StatusCode = (int)((HttpWebResponse)ex.Response).StatusCode; }
                else { this.StatusCode = 10; }
            }
        }
    }
}
