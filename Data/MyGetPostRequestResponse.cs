﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace SupportTools.Data
{
    public class MyGetPostRequestResponse
    {
        private WebRequest request;
        private Stream dataStream;
        public int Status { get; private set; }

        // Create a request using a URL that can receive a post.
        public MyGetPostRequestResponse(string url)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            request = WebRequest.Create(url);
        }

        // Set the Method property of the request to POST.
        public MyGetPostRequestResponse(string url, string method) : this(url)
        {
            if (method.Equals("GET") || method.Equals("POST"))
            { request.Method = method; }
            else
            { throw new Exception("Invalid Method Type"); }
        }


        public MyGetPostRequestResponse(string url, string method, string data) : this(url, method)
        {
            // Create POST data and convert it to a byte array.
            string postData = data;
            byte[] byteArray = Encoding.UTF8.GetBytes(postData);

            // Set the ContentType property of the WebRequest.
            request.ContentType = "application/x-www-form-urlencoded";

            // Set the ContentLength property of the WebRequest.
            request.ContentLength = byteArray.Length;

            // Get the request stream.
            dataStream = request.GetRequestStream();

            // Write the data to the request stream.
            dataStream.Write(byteArray, 0, byteArray.Length);

            // Close the Stream object.
            dataStream.Close();

        }

        public void MyGetResponse()
        {
            try
            {
                // Get the original response.
                WebResponse response = request.GetResponse();
                this.Status = (int)((HttpWebResponse)response).StatusCode;

                // Get the stream containing all content returned by the requested server.
                dataStream = response.GetResponseStream();

                // Open the stream using a StreamReader for easy access.
                StreamReader reader = new StreamReader(dataStream);

                // Read the content fully up to the end.
                string responseFromServer = reader.ReadToEnd();

                // Clean up the streams.
                reader.Close();
                dataStream.Close();
                response.Close();
            }
            catch (WebException ex)
            {
                if (ex.Response != null) { this.Status = (int)((HttpWebResponse)ex.Response).StatusCode; }
                else { this.Status = 10; }
            }
        }
    }
}

