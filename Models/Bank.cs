﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportTools.Models
{
    public class Bank
    {
        public string Id { get; set; }
        public string PublicName { get; set; }
        public string? mfo { get; set; }

    }
}
