﻿using System;

namespace SupportTools.Models
{
    public class Loan
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public int Status { get; set; }
        public string Request_Id { get; set; }
        public DateTime LastChangeDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public bool Returning { get; set; }
        public string PublicId { get; set; }
        public int System { get; set; }


    }
}
