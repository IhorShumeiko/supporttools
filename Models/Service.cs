﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportTools.Models
{
    public class Service
    {
        public string NameService { get; set; }
        public string UrlService { get; set; }
        public int? Code { get; set; }
        public DateTime StartFail { get; set; }
        public DateTime FinishFail { get; set; }
        public TimeSpan DeltaTime { get; set; }

        public Service(string name, string url, int? code, DateTime start, DateTime finish) 
        {
            this.NameService = name;
            this.UrlService = url;
            this.Code = code;
            this.StartFail = start;
            this.FinishFail = finish;
            this.DeltaTime = finish - start;
        }

        public override string ToString()
        { return this.UrlService + $"; Code = {this.Code}; Start = {StartFail}; PeriodFail = {DeltaTime}"; }
    }
}
