﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportTools.Models
{
    public class JsonObjForSender
    {
        public string site_name;
        public string error;
        public string url;
        public string chat_id;

        public JsonObjForSender(Site s, string chatId)
        {
            this.site_name = s.NameSite;
            this.error = s.Code.ToString();
            this.url = s.UrlSite;
            this.chat_id = chatId;
        }

    }
}
