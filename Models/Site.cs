﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SupportTools.Models
{
    public class Site
    {
        public string NameSite { get; set; }
        public string UrlSite { get; set; }
        public int? Code { get; set; }
        public DateTime StartFail { get; set; }
        public DateTime FinishFail { get; set; }
        public TimeSpan DeltaTime { get; set; }

        public Site(string name, string url, int? code, DateTime start, DateTime finish)
        {
            this.NameSite = name;
            this.UrlSite = url;
            this.Code = code;
            this.StartFail = start;
            this.FinishFail = finish;
            this.DeltaTime = finish - start;
        }

        public override string ToString()
        { return this.UrlSite + $"; Code = {this.Code}; Start = {StartFail}; PeriodFail = {DeltaTime}"; }
    }
}
